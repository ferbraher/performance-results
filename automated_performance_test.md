

# Auto generation of Triton performance results for each release

- [Description](#description)
- [Solutions](#solutions)
  - [Integration with a database and grafana](#integration-with-a-database-and-grafana)
  - [Updating wiki page from the CI pipeline](#updating-wiki-page-from-the-ci-pipeline)
  - [Hosting gitlab pages generated from this pipeline](#hosting-gitlab-pages-generated-from-this-pipeline)
  - [Using gitlab metrics reports](#using-gitlab-metrics-reports)

## Description

We are analyzing the possibility of automating the generation of the performance
results for each release.

Right now the performance results are computed within a manual pipeline that is
run during the release procedure. This results are manually uploaded to an excel
table with the historic of each release. This is not such a time consuming
procedure, but it could be automated, setting up the environment for some other
metrics and results that we may use in the future.

We have identified some possible solutions to the problem, each one of them with
his pros and cons, and these solutions are detailed in the next sections.

## Solutions

### Integration with a database and grafana

Grafana offers a powerful integration with gitlab CI. The documentation about
grafana integration with gitlab can be found in the gitlab documentation page
of [Grafana Dashboard Service](https://docs.gitlab.com/omnibus/settings/grafana.html).

Grafana dashboards could be integrated with a prometheus database, which could
be useful for time-series information. It is also possible to integrate it with
a SQL database in which the performance results can be stored and consulted.

| pros                                                                   | cons                                                            |
| ---------------------------------------------------------------------- | --------------------------------------------------------------- |
| Huge capabilities of monitoring the CI                                 | We should set up and maintain the gitlab omnibus infrastructure |
| Could be used for timeseries graph to monitor variables like CPU usage | Overkill for this simple task                                   |

### Updating wiki page from the CI pipeline

We could create a wiki page with the tests results. That way, we could edit the
page directly from the pipeline and update it with the results.

| pros                                              | cons                                                |
| ------------------------------------------------- | --------------------------------------------------- |
| Quite simple process                              | Gitlab runner must be able to push to the wiki repo |
| Possible to have some version control in the wiki | Viable for this simple use, but not to scalable     |

### Hosting gitlab pages generated from this pipeline

The gitlab pages are already implemented for Doxygen documentation. We could use
this space to show the performance results. We should know if there is a
possibility to have some persistance within the gitlab pages, so we can show the
results of previous tests.

| pros                                                                | cons                                                   |
| ------------------------------------------------------------------- | ------------------------------------------------------ |
| Easy to integrate within gitlab pipelines                           | We have to set up the environment for the gitlab pages |
| It is being already used by the automatic documentation of the code | We need a system to have persistance of versions       |


### Using gitlab metrics reports

This could be an easy option, but it requires a premium gitlab account to be
used. It shows a quick overview of the selected metrics directly on the MR.

| pros                             | cons                   |
| -------------------------------- | ---------------------- |
| Straight forward and easy to use | Premium account needed |



