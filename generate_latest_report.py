import pandas as pd
import os

def generate_latest_report(report_path, latest_reports_path, commit):
    report = pd.read_csv(report_path)
    if os.path.isfile(latest_reports_path):
        latest_reports = pd.read_csv(latest_reports_path)
    else:
        latest_reports = pd.read_csv("empty_report.csv")

    new_column = report.iloc[:,1]
    latest_reports.insert(2, commit, new_column)

    columns = latest_reports.columns.to_list()
    if len(columns) > 11:
        latest_reports.drop(columns=latest_reports.columns[-1], axis=1, inplace=True)

    print(latest_reports)
    latest_reports.to_csv(latest_reports_path, index=False)
    latest_reports.to_html("latest_reports.html", index=False)
    return latest_reports


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 4:
        generate_latest_report(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("2 arguments are required: report_path, latest_reports_path, commit")
    