import pandas as pd
import os

def generate_version_report(report_path, version_reports_path, commit):
    report = pd.read_csv(report_path)
    if os.path.isfile(version_reports_path):
        version_reports = pd.read_csv(version_reports_path)
    else:
        version_reports = pd.read_csv("empty_report.csv")

    new_column = report.iloc[:,1]
    version_reports.insert(2, commit, new_column)

    columns = version_reports.columns.to_list()
    if len(columns) > 11:
        version_reports.drop(columns=version_reports.columns[-1], axis=1, inplace=True)

    print(version_reports)
    version_reports.to_csv(version_reports_path, index=False)
    version_reports.to_html("version_reports.html", index=False)
    return version_reports


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 4:
        generate_version_report(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("2 arguments are required: report_path, version_reports_path, version")
    